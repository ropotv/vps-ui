import { IResource } from '@vps/models';

export function canExtractResource(
  resource: IResource | undefined | null
): boolean {
  return resource?.extractResourceJob?.job?.id == null;
}
