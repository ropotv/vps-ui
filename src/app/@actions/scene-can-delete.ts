import { IScene } from '@vps/models';

export function canDeleteScene(scene: IScene | undefined | null): boolean {
  return !!scene;
}
