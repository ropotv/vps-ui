import { IScene } from '@vps/models';

export function canMarkUnVerifiedScene(
  scene: IScene | undefined | null
): boolean {
  if (!scene) return false;
  return !!scene.verifiedAt;
}
