import { IVideo } from '@vps/models';

export function canDownloadVideo(video: IVideo | undefined | null): boolean {
  return video?.videoFile != null || video?.thumbnail?.file != null;
}
