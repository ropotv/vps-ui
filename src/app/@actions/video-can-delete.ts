import { IVideo } from '@vps/models';

export function canDeleteVideo(video: IVideo | undefined | null): boolean {
  return !!video;
}
