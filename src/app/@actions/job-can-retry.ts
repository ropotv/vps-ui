import { IJob, JobStatus } from '@vps/models';

export function canRetryJob(job: IJob | undefined | null): boolean {
  if (!job) return false;
  return job.status === JobStatus.Failed;
}
