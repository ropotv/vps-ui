import { IThumbnail } from '@vps/models';

export function canDeleteThumbnail(
  thumbnail: IThumbnail | undefined | null
): boolean {
  return !!thumbnail;
}
