import { IJob } from '@vps/models';

export function getJobType(job: IJob | undefined | null): string {
  if (!job) return '';
  if (job.compileScenesJob) {
    return 'Merge Scenes';
  }
  if (job.compileVideoJob) {
    return 'Compile Video';
  }
  if (job.extractResourceJob) {
    return 'Extract Resource';
  }

  return 'N/A';
}
