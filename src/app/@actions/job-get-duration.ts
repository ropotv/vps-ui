import { IJob } from '@vps/models';

export function getJobDuration(job: IJob | undefined | null): number | null {
  if (!job) return null;
  if (!job.startedAt) {
    return null;
  }
  if (!job.finishedAt) {
    const start = new Date(job.startedAt).getTime();
    const end = new Date().getTime();
    return end > start ? end - start : null;
  }

  const start = new Date(job.startedAt).getTime();
  const end = new Date(job.finishedAt).getTime();
  return end > start ? end - start : 0;
}
