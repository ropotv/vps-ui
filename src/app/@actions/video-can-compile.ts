import { IVideo } from '@vps/models';

export function canCompileVideo(video: IVideo | undefined | null): boolean {
  return video?.publishedAt == null && video?.compileVideoJob?.job?.id == null;
}
