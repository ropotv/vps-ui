import { IVideo, JobStatus } from '@vps/models';

export function canPublishVideo(video: IVideo | undefined | null): boolean {
  return (
    video?.publishedAt == null &&
    video?.compileVideoJob?.job?.status === JobStatus.Done
  );
}
