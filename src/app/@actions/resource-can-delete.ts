import { IResource } from '@vps/models';

export function canDeleteResource(
  resource: IResource | undefined | null
): boolean {
  return !!resource;
}
