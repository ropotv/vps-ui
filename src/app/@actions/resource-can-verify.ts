import { IResource, JobStatus } from '@vps/models';

export function canVerifyResource(
  resource: IResource | undefined | null
): boolean {
  return (
    resource?.extractResourceJob?.job?.status === JobStatus.Done &&
    !resource?.isVerified
  );
}
