import { IChannel } from '@vps/models';

export function canDeleteChannel(
  channel: IChannel | undefined | null
): boolean {
  return !!channel;
}
