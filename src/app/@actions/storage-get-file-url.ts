import { IStorageFile } from '@vps/models';

export function getStorageFileUrl(file: IStorageFile | undefined): string {
  if (!file?.bucketName || !file?.fileName) {
    return 'https://knetic.org.uk/wp-content/uploads/2020/07/Video-Placeholder.png';
  }

  return `https://storage.googleapis.com/${file.bucketName}/${file.fileName}`;
}
