export interface IApiQuery {
  size?: number;
  page?: number;

  filter?: string;
  order?: string;

  [k: string]: any;
}
