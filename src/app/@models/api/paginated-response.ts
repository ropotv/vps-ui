export type IPaginatedResponse<T> = [T[], number];
