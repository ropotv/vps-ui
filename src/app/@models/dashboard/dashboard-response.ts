export interface IDashboardResponse {
  resourcesAllAmount: number;
  resourcesAllSize: number;
  resourcesProcessedAmount: number;
  scenesAllSize: number;
  scenesAllDuration: number;
  scenesAllAmount: number;
  scenesVerifiedAmount: number;
  scenesUsedAmount: number;
  videosAllAmount: number;
  videosAllSize: number;
  videosAllDuration: number;
  jobsProcessingAmount: number;
  jobsInQueueAmount: number;
  jobsFailedAmount: number;
  jobsDoneAmount: number;
}
