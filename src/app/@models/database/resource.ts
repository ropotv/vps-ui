import { IJobCompileScenes } from './job-compile-scenes';
import { IJobExtractResource } from './job-extract-resource';
import { IScene } from './scene';
import { IStorageFile } from './storage-file';

export interface IResource {
  id: number;
  createdAt: string;
  isVerified: boolean;

  videoFile?: IStorageFile | undefined;
  scenes?: IScene[] | undefined;
  compileScenesJobs?: IJobCompileScenes[] | undefined;
  extractResourceJob?: IJobExtractResource | undefined;
}
