import { IJobCompileScenes } from './job-compile-scenes';
import { IResource } from './resource';
import { IStorageFile } from './storage-file';
import { IVideo } from './video';

export interface IScene {
  id: number;
  createdAt: string;
  verifiedAt: string;
  duration: number;

  imageFile?: IStorageFile | undefined;
  videoFile?: IStorageFile | undefined;
  resource?: IResource | undefined;
  videos?: IVideo[] | undefined;
  compileScenesJob?: IJobCompileScenes | undefined;
}
