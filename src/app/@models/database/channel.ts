import { IVideo } from '@vps/models';

export interface IChannel {
  id: number;
  createdAt: string;
  name: string;
  priority: number;

  videos?: IVideo[] | undefined;
}
