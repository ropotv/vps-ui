export interface IStorageFile {
  id: number;
  createdAt: string;
  bucketName: string;
  fileName: string;
  size: number;
}
