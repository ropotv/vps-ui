import { IChannel } from './channel';
import { IJobCompileVideo } from './job-compile-video';
import { IScene } from './scene';
import { IStorageFile } from './storage-file';
import { IThumbnail } from './thumbnail';

export interface IVideo {
  id: number;
  createdAt: string;
  publishedAt: string;
  duration: number;
  width: number;
  height: number;
  orderedSceneIds: string;

  thumbnail?: IThumbnail | undefined;
  channel?: IChannel | undefined;
  videoFile?: IStorageFile | undefined;
  scenes?: IScene[] | undefined;
  compileVideoJob?: IJobCompileVideo | undefined;
}
