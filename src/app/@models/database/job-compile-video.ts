import { IJob } from './job';
import { IVideo } from './video';

export interface IJobCompileVideo {
  id: number;
  createdAt: string;

  job?: IJob | undefined;
  video?: IVideo | undefined;
}
