import { IJob } from './job';
import { IResource } from './resource';

export interface IJobExtractResource {
  id: number;
  createdAt: string;

  job?: IJob | undefined;
  resource?: IResource | undefined;
}
