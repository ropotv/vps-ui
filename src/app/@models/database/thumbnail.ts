import { IStorageFile } from './storage-file';
import { IVideo } from './video';

export interface IThumbnail {
  id: number;
  createdAt: string;

  file?: IStorageFile;
  videos?: IVideo[];
}
