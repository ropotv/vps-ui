import { IJobCompileScenes } from './job-compile-scenes';
import { IJobCompileVideo } from './job-compile-video';
import { IJobExtractResource } from './job-extract-resource';
import { IJobLog } from './job-log';

export interface IJob {
  id: number;
  createdAt: string;
  status: JobStatus;
  startedAt: string;
  finishedAt: string;
  priority: number;

  logs?: IJobLog[] | undefined;
  compileScenesJob?: IJobCompileScenes | undefined;
  compileVideoJob?: IJobCompileVideo | undefined;
  extractResourceJob?: IJobExtractResource | undefined;
}

export enum JobStatus {
  InQueue = 'in-queue',
  Processing = 'processing',
  Failed = 'failed',
  Done = 'done',
}
