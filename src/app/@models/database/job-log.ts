import { IJob } from './job';

export interface IJobLog {
  id: number;
  createdAt: string;
  severity: JobLogSeverity;
  message: string;

  job?: IJob | undefined;
}

export enum JobLogSeverity {
  Info = 'info',
  Success = 'success',
  Error = 'error',
}
