import { IJob } from './job';
import { IResource } from './resource';
import { IScene } from './scene';

export interface IJobCompileScenes {
  id: number;
  createdAt: string;

  job?: IJob | undefined;
  resource?: IResource | undefined;
  scenes?: IScene[] | undefined;
}
