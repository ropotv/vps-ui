import { Injectable } from '@angular/core';
import { AppCache } from '@vps/cache';
import { IUserProfile } from '@vps/models';
import { BehaviorSubject, Observable } from 'rxjs';

export interface IStoreState {
  authToken: string | null;
  profile: IUserProfile | null;
}

function getDefaultState(): IStoreState {
  return {
    authToken: AppCache.getCache('vps-ui.token'),
    profile: null,
  };
}

@Injectable({
  providedIn: 'root',
})
export class AppStore {
  private _state$ = new BehaviorSubject<IStoreState>(getDefaultState());

  public patchState(partialState: Partial<IStoreState>): void {
    this._state$.next({ ...this._state$.value, ...partialState });
  }

  public resetState(): void {
    this._state$.next({ ...getDefaultState() });
  }

  public get get(): IStoreState {
    return this._state$.value;
  }

  public get state$(): Observable<IStoreState> {
    return this._state$.asObservable();
  }
}
