import { CommonModule } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule],
})
export class VideoPlayerComponent implements OnChanges {
  @Input()
  public videoSrc: string | undefined;

  @ViewChild('videoElement')
  private _videoElement: ElementRef<HTMLVideoElement> | undefined;

  public hasError = false;

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes['videoSrc']) {
      this.hasError = false;
      setTimeout(() => {
        this._videoElement?.nativeElement?.load();
        this._videoElement?.nativeElement?.play();
      });
    }
  }
}
