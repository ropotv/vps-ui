import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule],
})
export class AvatarComponent {
  @Input()
  public name: string | undefined;

  @Input()
  public image: string | undefined;

  @Input()
  public roundedClass = 'rounded-full';

  @Input()
  public size = 40;
}
