import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import {
  MatDialog,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-dialog-layout',
  templateUrl: './dialog-layout.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [MatDialogModule, MatButtonModule, MatIconModule],
})
export class DialogLayoutComponent implements OnDestroy {
  constructor(
    private _dialog: MatDialog,
    private _router: Router,
    private _route: ActivatedRoute
  ) {}

  @Input()
  public width = '50%';

  @Input()
  public height = '50%';

  @Input()
  public caption: string | undefined;

  @Input()
  public onCloseRoute: string | undefined;

  @Output()
  public onClose = new EventEmitter<void>();

  private _dialogRef: MatDialogRef<any> | undefined;

  @ViewChild('template')
  private set _template(template: TemplateRef<any>) {
    this._dialogRef = this._dialog.open(template, {
      disableClose: true,
      width: this.width,
      height: this.height,
      maxWidth: '100vw',
      maxHeight: '100vh',
    });
    this._dialogRef.afterClosed().subscribe((emitEvent) => {
      if (emitEvent) {
        if (this.onCloseRoute) {
          this._router.navigate([this.onCloseRoute], {
            relativeTo: this._route,
          });
        }

        this.onClose.emit();
      }
    });
  }

  public ngOnDestroy(): void {
    this._dialogRef?.close(false);
  }

  public close(): void {
    this._dialogRef?.close(true);
  }
}
