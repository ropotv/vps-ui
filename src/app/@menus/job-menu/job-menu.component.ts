import { NgIf } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSnackBar } from '@angular/material/snack-bar';
import { canRetryJob } from '@vps/actions';
import { JobsProvider } from '@vps/api';
import { AlertService } from '@vps/features';
import { IJob } from '@vps/models';

@Component({
  selector: 'app-job-menu',
  templateUrl: './job-menu.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [MatButtonModule, NgIf, MatMenuModule, MatIconModule],
})
export class JobMenuComponent {
  constructor(
    private _alert: AlertService,
    private _snackBar: MatSnackBar,
    private _jobsProvider: JobsProvider
  ) {}

  public canRetryJob = canRetryJob;

  @Input()
  public job: IJob | undefined;

  @Input()
  public elementClass: string | undefined;

  @Output()
  public afterRetry = new EventEmitter<void>();

  public onRetry(job: IJob) {
    this._alert
      .confirm({
        caption: 'Retry Job?',
        description: 'Are you sure to retry job?',
        acceptBtn: 'Retry',
      })
      .subscribe(async () => {
        await this._jobsProvider.retryJob(job.id);
        this._snackBar.open('Job was retried and moved in queue');
        this.afterRetry.emit();
      });
  }
}
