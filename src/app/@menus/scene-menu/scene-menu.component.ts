import { NgIf } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  canDeleteScene,
  canMarkUnVerifiedScene,
  canMarkVerifiedScene,
} from '@vps/actions';
import { ScenesProvider } from '@vps/api';
import { AlertService } from '@vps/features';
import { IScene } from '@vps/models';

@Component({
  selector: 'app-scene-menu',
  templateUrl: './scene-menu.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [MatButtonModule, NgIf, MatMenuModule, MatIconModule],
})
export class SceneMenuComponent {
  constructor(
    private _alert: AlertService,
    private _snackBar: MatSnackBar,
    private _scenesProvider: ScenesProvider
  ) {}

  public canMarkVerifiedScene = canMarkVerifiedScene;
  public canMarkUnVerifiedScene = canMarkUnVerifiedScene;
  public canDeleteScene = canDeleteScene;

  @Input()
  public scene: IScene | undefined;

  @Input()
  public elementClass: string | undefined;

  @Output()
  public afterVerify = new EventEmitter<void>();

  @Output()
  public afterUnVerify = new EventEmitter<void>();

  @Output()
  public afterDelete = new EventEmitter<void>();

  public onMarkAsVerified(scene: IScene) {
    this._alert
      .confirm({
        caption: 'Mark as Verified?',
        description: 'Are you sure to mark as verified this scene?',
        acceptBtn: 'Yes',
      })
      .subscribe(async () => {
        await this._scenesProvider.updateScene(scene.id, { isVerified: true });
        this._snackBar.open('Scene was marked as verified!');
        this.afterVerify.emit();
      });
  }

  public onMarkAsUnVerified(scene: IScene) {
    this._alert
      .confirm({
        caption: 'Mark as Unverified?',
        description: 'Are you sure to mark as unverified this scene?',
        acceptBtn: 'Yes',
      })
      .subscribe(async () => {
        await this._scenesProvider.updateScene(scene.id, { isVerified: false });
        this._snackBar.open('Scene was marked as unverified!');
        this.afterUnVerify.emit();
      });
  }

  public onDelete(scene: IScene) {
    this._alert
      .confirm({
        caption: 'Delete Scene?',
        description: 'Are you sure to delete this scene?',
        acceptBtn: 'Delete Scene',
      })
      .subscribe(async () => {
        await this._scenesProvider.deleteScene(scene.id);
        this._snackBar.open('Scene was deleted successfully!');
        this.afterDelete.emit();
      });
  }
}
