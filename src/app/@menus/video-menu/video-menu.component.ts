import { CommonModule } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  canCompileVideo,
  canDeleteVideo,
  canDownloadVideo,
  canPublishVideo,
  getStorageFileUrl,
} from '@vps/actions';
import { UtilsProvider, VideosProvider } from '@vps/api';
import { AlertService } from '@vps/features';
import { IChannel, IVideo } from '@vps/models';
import { downloadFile } from '@vps/utils';
import slugify from 'slugify';
@Component({
  selector: 'app-video-menu',
  templateUrl: './video-menu.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, MatButtonModule, MatIconModule, MatMenuModule],
})
export class VideoMenuComponent {
  constructor(
    private _alert: AlertService,
    private _snackBar: MatSnackBar,
    private _videosProvider: VideosProvider,
    private _utilsProvider: UtilsProvider
  ) {}

  public canDownloadVideo = canDownloadVideo;
  public canCompileVideo = canCompileVideo;
  public canPublishVideo = canPublishVideo;
  public canDeleteVideo = canDeleteVideo;

  @Input()
  public video: IVideo | undefined;

  @Input()
  public channel: IChannel | undefined;

  @Input()
  public elementClass: string | undefined;

  @Output()
  public afterCompile = new EventEmitter<void>();

  @Output()
  public afterPublish = new EventEmitter<void>();

  @Output()
  public afterDelete = new EventEmitter<void>();

  public onCompile(video: IVideo) {
    this._alert
      .confirm({
        caption: 'Compile video?',
        description: 'Are you sure to compile this video?',
        acceptBtn: 'Compile',
      })
      .subscribe(async () => {
        await this._videosProvider.compileVideo(video.id);
        this._snackBar.open('Video was scheduled for compilation');
        this.afterCompile.emit();
      });
  }

  public async onDownload(video: IVideo) {
    const prefix = slugify(`${this.channel!.name} #${video.id}`);

    if (video.videoFile) {
      const videoBlob = await this._utilsProvider.downloadBlob({
        url: getStorageFileUrl(video.videoFile),
      });
      downloadFile(videoBlob, `${prefix}.mp4`);
    }
    if (video.thumbnail?.file) {
      const thumbnailBlob = await this._utilsProvider.downloadBlob({
        url: getStorageFileUrl(video.thumbnail.file),
      });
      downloadFile(thumbnailBlob, `${prefix}.jpg`);
    }
  }

  public onPublish(video: IVideo) {
    this._alert
      .confirm({
        caption: 'Publish video?',
        description: 'Are you sure to publish this video?',
        acceptBtn: 'Publish',
      })
      .subscribe(async () => {
        await this._videosProvider.updateVideo(video.id, {
          publishedAt: new Date().toISOString(),
        });
        this._snackBar.open('Video was published');
        this.afterPublish.emit();
      });
  }

  public onDelete(video: IVideo) {
    this._alert
      .confirm({
        caption: 'Delete Video?',
        description: 'Are you sure to delete this video?',
        acceptBtn: 'Delete Video',
      })
      .subscribe(async () => {
        await this._videosProvider.deleteVideo(video.id);
        this._snackBar.open('Video was deleted successfully!');
        this.afterDelete.emit();
      });
  }
}
