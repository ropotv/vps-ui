import { CommonModule } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSnackBar } from '@angular/material/snack-bar';
import { canDeleteChannel } from '@vps/actions';
import { ChannelsProvider } from '@vps/api';
import { AlertService } from '@vps/features';
import { IChannel } from '@vps/models';

@Component({
  selector: 'app-channel-menu',
  templateUrl: './channel-menu.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, MatButtonModule, MatIconModule, MatMenuModule],
})
export class ChannelMenuComponent {
  constructor(
    private _alert: AlertService,
    private _snackBar: MatSnackBar,
    private _channelsProvider: ChannelsProvider
  ) {}

  public canDeleteChannel = canDeleteChannel;

  @Input()
  public channel: IChannel | undefined;

  @Input()
  public elementClass: string | undefined;

  @Output()
  public afterDelete = new EventEmitter<void>();

  public onDelete(channel: IChannel) {
    this._alert
      .confirm({
        caption: 'Delete Channel?',
        description:
          'Are you sure to delete this channel? This action will remove all videos from it',
        acceptBtn: 'Delete Channel',
      })
      .subscribe(async () => {
        await this._channelsProvider.deleteChannel(channel.id);
        this._snackBar.open('Channel was deleted successfully!');
        this.afterDelete.emit();
      });
  }
}
