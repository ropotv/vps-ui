import { NgIf } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RouterLink } from '@angular/router';
import {
  canDeleteResource,
  canExtractResource,
  canVerifyResource,
} from '@vps/actions';
import { ResourcesProvider } from '@vps/api';
import { AlertService } from '@vps/features';
import { IResource } from '@vps/models';

@Component({
  selector: 'app-resource-menu',
  templateUrl: './resource-menu.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [NgIf, RouterLink, MatButtonModule, MatMenuModule, MatIconModule],
})
export class ResourceMenuComponent {
  constructor(
    private _alert: AlertService,
    private _snackBar: MatSnackBar,
    private _resourcesProvider: ResourcesProvider
  ) {}

  public canExtractResource = canExtractResource;
  public canVerifyResource = canVerifyResource;
  public canDeleteResource = canDeleteResource;

  @Input()
  public resource: IResource | undefined;

  @Input()
  public elementClass: string | undefined;

  @Output()
  public afterExtract = new EventEmitter<void>();

  @Output()
  public afterDelete = new EventEmitter<void>();

  public onExtract(resource: IResource): void {
    this._alert
      .confirm({
        caption: 'Extract resource',
        description: 'Do you confirm extract scenes from this resource?',
        acceptBtn: 'Extract',
      })
      .subscribe(async () => {
        await this._resourcesProvider.extractScenes(resource.id);
        this._snackBar.open('Resource was scheduled for extraction!');
        this.afterExtract.emit();
      });
  }

  public onDelete(resource: IResource) {
    this._alert
      .confirm({
        caption: 'Delete resource',
        description: 'Do you confirm deleting this resource?',
        acceptBtn: 'Delete',
      })
      .subscribe(async () => {
        await this._resourcesProvider.deleteResource(resource.id);
        this._snackBar.open('Resource was deleted successfully!');
        this.afterDelete.emit();
      });
  }
}
