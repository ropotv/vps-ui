import { NgIf } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSnackBar } from '@angular/material/snack-bar';
import { canDeleteThumbnail } from '@vps/actions';
import { ThumbnailsProvider } from '@vps/api';
import { AlertService } from '@vps/features';
import { IThumbnail } from '@vps/models';

@Component({
  selector: 'app-thumbnail-menu',
  templateUrl: './thumbnail-menu.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [MatButtonModule, NgIf, MatMenuModule, MatIconModule],
})
export class ThumbnailMenuComponent {
  constructor(
    private _alert: AlertService,
    private _snackBar: MatSnackBar,
    private _thumbnailsProvider: ThumbnailsProvider
  ) {}

  public canDeleteThumbnail = canDeleteThumbnail;

  @Input()
  public thumbnail: IThumbnail | undefined;

  @Input()
  public elementClass: string | undefined;

  @Output()
  public afterDelete = new EventEmitter<void>();

  public onDelete(thumbnail: IThumbnail) {
    this._alert
      .confirm({
        caption: 'Delete Thumbnail?',
        description: 'Are you sure to delete this thumbnail?',
        acceptBtn: 'Delete Thumbnail',
      })
      .subscribe(async () => {
        await this._thumbnailsProvider.deleteThumbnail(thumbnail.id);
        this._snackBar.open('Thumbnail was deleted successfully!');
        this.afterDelete.emit();
      });
  }
}
