export function downloadFile(blob: Blob, fileName: string): void {
  const link = document.createElement('a');
  link.download = fileName;
  link.href = URL.createObjectURL(blob);
  link.style.position = 'fixed';
  link.style.bottom = '0px';
  link.style.right = '0px';

  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
}
