import { IApiQuery } from '@vps/models';

export const buildApiQuery = (params: IApiQuery): string => {
  let query = '';
  for (const key in params) {
    if (params.hasOwnProperty(key)) {
      const proto = params[key];
      query = `${query}&${key}=${proto}`;
    }
  }
  return `?${query.substring(1)}`;
};
