import { inject } from '@angular/core';
import { AppEvents, ChangeType } from '@vps/events';

interface IEventDetector {
  markAsChanged(): void;
  detectChanges(): void;
}

export function getEventDetector(type: ChangeType): IEventDetector {
  const _events = inject(AppEvents);
  let hasChanges = false;

  function markAsChanged() {
    hasChanges = true;
  }

  function detectChanges() {
    if (hasChanges) {
      _events.publish(type);
    }
  }

  return { markAsChanged, detectChanges };
}
