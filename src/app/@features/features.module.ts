import { NgModule } from '@angular/core';
import { AlertModule } from './alert';

@NgModule({
  imports: [AlertModule],
})
export class FeaturesModule {}
