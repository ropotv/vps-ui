import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { filter, Observable } from 'rxjs';
import { AlertConfirmComponent, IAlertConfirmConfig } from './alert-confirm';

@Injectable({
  providedIn: 'root',
})
export class AlertService {
  constructor(private _dialog: MatDialog) {}

  public confirm(data: IAlertConfirmConfig): Observable<boolean> {
    return this._dialog
      .open(AlertConfirmComponent, {
        data,
        position: { top: '20px' },
        width: '500px',
      })
      .afterClosed()
      .pipe(filter(Boolean));
  }
}
