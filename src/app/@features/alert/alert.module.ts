import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { AlertConfirmComponent } from './alert-confirm';

@NgModule({
  imports: [CommonModule, MatDialogModule, MatButtonModule],
  declarations: [AlertConfirmComponent],
  exports: [AlertConfirmComponent],
})
export class AlertModule {}
