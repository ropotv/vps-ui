import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IAlertConfirmConfig } from './alert-confirm.interface';

@Component({
  templateUrl: './alert-confirm.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AlertConfirmComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: IAlertConfirmConfig) {}
}
