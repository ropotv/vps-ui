export interface IAlertConfirmConfig {
  caption: string;
  description: string;
  acceptBtn?: string;
  declineBtn?: string;
}
