import { Pipe, PipeTransform } from '@angular/core';
import { IResource, JobStatus } from '@vps/models';

const MappedData = {
  [JobStatus.Failed]: 'Failed',
  [JobStatus.Processing]: 'Extracting',
  [JobStatus.InQueue]: 'In-Queue',
  [JobStatus.Done]: 'Extracted',
};

@Pipe({ name: 'resourceStatusText', standalone: true })
export class ResourceStatusTextPipe implements PipeTransform {
  public transform(resource: IResource | undefined | null): string {
    if (!resource) return '';
    console.log(resource);
    if (resource.isVerified) {
      return 'Verified';
    }
    if (resource.extractResourceJob?.job?.status) {
      return MappedData[resource.extractResourceJob.job.status!];
    }

    return 'Uploaded';
  }
}
