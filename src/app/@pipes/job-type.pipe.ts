import { Pipe, PipeTransform } from '@angular/core';
import { getJobType } from '@vps/actions';
import { IJob } from '@vps/models';

@Pipe({ name: 'jobType', standalone: true })
export class JobTypePipe implements PipeTransform {
  public transform(job: IJob | undefined | null): string {
    return getJobType(job);
  }
}
