import { Pipe, PipeTransform } from '@angular/core';
import { IJobLog, JobLogSeverity } from '@vps/models';

const MappedData = {
  [JobLogSeverity.Info]: 'info',
  [JobLogSeverity.Error]: 'warning',
  [JobLogSeverity.Success]: 'verified',
};

@Pipe({ name: 'jobLogIcon', standalone: true })
export class JobLogIconPipe implements PipeTransform {
  public transform(log: IJobLog | undefined | null): string {
    if (!log) return '';
    return MappedData[log.severity];
  }
}
