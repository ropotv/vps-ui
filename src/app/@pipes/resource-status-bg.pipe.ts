import { Pipe, PipeTransform } from '@angular/core';
import { IResource, JobStatus } from '@vps/models';

const MappedData = {
  [JobStatus.Failed]: 'bg-warn-500',
  [JobStatus.Processing]: 'bg-primary-500',
  [JobStatus.InQueue]: 'bg-slate-500',
  [JobStatus.Done]: 'bg-success-500',
};

@Pipe({ name: 'resourceStatusBg', standalone: true })
export class ResourceStatusBgPipe implements PipeTransform {
  public transform(resource: IResource | undefined | null): string {
    if (!resource) return '';

    if (resource.isVerified) {
      return 'bg-accent-500';
    }
    if (resource.extractResourceJob?.job?.status) {
      return MappedData[resource.extractResourceJob.job.status];
    }

    return 'bg-slate-400';
  }
}
