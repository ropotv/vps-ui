import { Pipe, PipeTransform } from '@angular/core';
import { getJobDuration } from '@vps/actions';
import { IJob } from '@vps/models';

@Pipe({ name: 'jobDuration', standalone: true })
export class JobDurationPipe implements PipeTransform {
  public transform(job: IJob | undefined | null): number | null {
    return getJobDuration(job);
  }
}
