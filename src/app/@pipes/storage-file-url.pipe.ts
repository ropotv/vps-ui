import { Pipe, PipeTransform } from '@angular/core';
import { getStorageFileUrl } from '@vps/actions';
import { IStorageFile } from '@vps/models';

@Pipe({ name: 'storageFileUrl', standalone: true })
export class StorageFileUrlPipe implements PipeTransform {
  public transform(file: IStorageFile | undefined): string {
    return getStorageFileUrl(file);
  }
}
