import { Pipe, PipeTransform } from '@angular/core';
import { IJob, JobStatus } from '@vps/models';

const MappedData = {
  [JobStatus.Failed]: 'bg-warn-500',
  [JobStatus.Processing]: 'bg-primary-500',
  [JobStatus.InQueue]: 'bg-slate-500',
  [JobStatus.Done]: 'bg-success-500',
};

@Pipe({ name: 'jobStatusBg', standalone: true })
export class JobStatusBgPipe implements PipeTransform {
  public transform(job: IJob | undefined | null): string {
    if (!job) return '';
    return MappedData[job.status];
  }
}
