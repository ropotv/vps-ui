import { Pipe, PipeTransform } from '@angular/core';
import { IVideo, JobStatus } from '@vps/models';

const MappedData = {
  [JobStatus.Failed]: 'bg-warn-500',
  [JobStatus.Processing]: 'bg-primary-500',
  [JobStatus.InQueue]: 'bg-slate-500',
  [JobStatus.Done]: 'bg-success-500',
};

@Pipe({ name: 'videoStatusBg', standalone: true })
export class VideoStatusBgPipe implements PipeTransform {
  public transform(video: IVideo | undefined | null): string {
    if (!video) return '';

    if (video.publishedAt) {
      return 'bg-accent-500';
    }
    if (video.compileVideoJob?.job?.status) {
      return MappedData[video.compileVideoJob?.job?.status];
    }

    return 'bg-slate-400';
  }
}
