import { Pipe, PipeTransform } from '@angular/core';
import prettyMilliseconds, { Options } from 'pretty-ms';

@Pipe({ name: 'formatMilliseconds', standalone: true })
export class FormatMillisecondsPipe implements PipeTransform {
  public transform(
    milliseconds: number | undefined | null,
    options: Options = { unitCount: 2 }
  ): string | null {
    if (milliseconds == null) return null;
    return prettyMilliseconds(milliseconds, options);
  }
}
