import { Pipe, PipeTransform } from '@angular/core';
import { IVideo, JobStatus } from '@vps/models';

const MappedData = {
  [JobStatus.Failed]: 'Failed',
  [JobStatus.Processing]: 'Compiling',
  [JobStatus.InQueue]: 'In-Queue',
  [JobStatus.Done]: 'Compiled',
};

@Pipe({ name: 'videoStatusText', standalone: true })
export class VideoStatusTextPipe implements PipeTransform {
  public transform(video: IVideo | undefined | null): string {
    if (!video) return '';

    if (video.publishedAt) {
      return 'Published';
    }
    if (video.compileVideoJob?.job?.status) {
      return MappedData[video.compileVideoJob?.job?.status!];
    }

    return 'Draft';
  }
}
