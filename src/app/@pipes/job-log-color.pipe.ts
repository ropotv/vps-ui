import { Pipe, PipeTransform } from '@angular/core';
import { IJobLog, JobLogSeverity } from '@vps/models';

const MappedData = {
  [JobLogSeverity.Info]: 'text-slate-500',
  [JobLogSeverity.Error]: 'text-warn-500',
  [JobLogSeverity.Success]: 'text-success-500',
};

@Pipe({ name: 'jobLogColor', standalone: true })
export class JobLogColorPipe implements PipeTransform {
  public transform(log: IJobLog | undefined | null): string {
    if (!log) return '';
    return MappedData[log.severity];
  }
}
