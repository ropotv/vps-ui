import { Pipe, PipeTransform } from '@angular/core';
import { formatBytes } from '@vps/utils';

@Pipe({ name: 'formatBytes', standalone: true })
export class FormatBytesPipe implements PipeTransform {
  public transform(bytes: number | undefined | null, decimals = 2): string {
    return formatBytes(bytes, decimals);
  }
}
