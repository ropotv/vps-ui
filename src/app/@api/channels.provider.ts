import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IApiQuery, IChannel, IPaginatedResponse } from '@vps/models';
import { buildApiQuery } from '@vps/utils';
import { firstValueFrom } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ChannelsProvider {
  private _endpoint: string = '/channels';

  constructor(private _http: HttpClient) {}

  public getChannels(params: IApiQuery): Promise<IPaginatedResponse<IChannel>> {
    const query = buildApiQuery(params);
    const url = `${this._endpoint}${query}`;
    return firstValueFrom(this._http.get<IPaginatedResponse<IChannel>>(url));
  }

  public createChannel(body: ICreateChannelBody): Promise<IChannel> {
    const url = `${this._endpoint}`;
    return firstValueFrom(this._http.post<IChannel>(url, body));
  }

  public getChannel(channelId: number): Promise<IChannel> {
    const url = `${this._endpoint}/${channelId}`;
    return firstValueFrom(this._http.get<IChannel>(url));
  }

  public updateChannel(
    channelId: number,
    body: IUpdateChannelBody
  ): Promise<IChannel> {
    const url = `${this._endpoint}/${channelId}`;
    return firstValueFrom(this._http.patch<IChannel>(url, body));
  }

  public deleteChannel(channelId: number): Promise<void> {
    const url = `${this._endpoint}/${channelId}`;
    return firstValueFrom(this._http.delete<void>(url));
  }

  public reorderChannel(
    channelId: number,
    body: IReorderChannelBody
  ): Promise<void> {
    const url = `${this._endpoint}/${channelId}/reorder`;
    return firstValueFrom(this._http.post<void>(url, body));
  }
}

export interface ICreateChannelBody {
  name: string;
}

export interface IUpdateChannelBody {
  name?: string;
}

export interface IReorderChannelBody {
  predecessorPriority: number | null;
  successorPriority: number | null;
}
