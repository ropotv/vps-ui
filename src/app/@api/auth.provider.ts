import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IAuthLogin, IAuthResponse, IUserProfile } from '@vps/models';
import { firstValueFrom } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AuthProvider {
  private _endpoint: string = '/auth';

  constructor(private _http: HttpClient) {}

  public login(data: IAuthLogin): Promise<IAuthResponse> {
    const url = `${this._endpoint}/login`;
    return firstValueFrom(this._http.post<IAuthResponse>(url, data));
  }

  public getProfile(): Promise<IUserProfile> {
    const url = `${this._endpoint}/profile`;
    return firstValueFrom(this._http.get<IUserProfile>(url));
  }
}
