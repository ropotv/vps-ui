import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from '@vps/env';
import { ApiHttpCode } from '@vps/models';
import { AppStore } from '@vps/store';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from './auth.service';

export class ApiInterceptor implements HttpInterceptor {
  constructor(
    private _store: AppStore,
    private _authService: AuthService,
    private _snackBar: MatSnackBar
  ) {}

  public intercept(
    request: HttpRequest<any>,
    handler: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (request.url.includes('/assets/')) {
      return handler.handle(request);
    }

    return handler
      .handle(this._prepareRequest(request))
      .pipe(catchError((err) => this._handleError(err)));
  }

  private _prepareRequest(request: HttpRequest<any>): HttpRequest<any> {
    let headers = request.headers;

    if (this._store.get.authToken) {
      headers = headers.set(
        'Authorization',
        `Bearer ${this._store.get.authToken}`
      );
    }

    return request.clone({
      headers,
      url: environment.api.concat(request.url),
    });
  }

  private _handleError(err: HttpErrorResponse): Observable<any> {
    switch (err.status) {
      case ApiHttpCode.Unauthorized:
        this._authService.logout();
        break;
      case ApiHttpCode.BadRequest:
      case ApiHttpCode.Forbidden:
      case ApiHttpCode.NotFound:
      case ApiHttpCode.Conflict:
      case ApiHttpCode.Error:
        this._snackBar.open(err?.error?.message, 'Close', {
          duration: 20000,
          politeness: 'polite',
        });
        break;
    }

    return throwError(() => err);
  }
}
