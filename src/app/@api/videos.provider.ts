import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IApiQuery, IPaginatedResponse, IVideo } from '@vps/models';
import { buildApiQuery } from '@vps/utils';
import { firstValueFrom } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class VideosProvider {
  private _endpoint: string = '/videos';

  constructor(private _http: HttpClient) {}

  public getVideos(params: IApiQuery): Promise<IPaginatedResponse<IVideo>> {
    const query = buildApiQuery(params);
    const url = `${this._endpoint}${query}`;
    return firstValueFrom(this._http.get<IPaginatedResponse<IVideo>>(url));
  }

  public countVideos(params: IApiQuery): Promise<number> {
    const query = buildApiQuery(params);
    const url = `${this._endpoint}/count${query}`;
    return firstValueFrom(this._http.get<number>(url));
  }

  public createVideo(body: ICreateVideoBody): Promise<IVideo> {
    const url = `${this._endpoint}`;
    return firstValueFrom(this._http.post<IVideo>(url, body));
  }

  public getVideo(videoId: number): Promise<IVideo> {
    const url = `${this._endpoint}/${videoId}`;
    return firstValueFrom(this._http.get<IVideo>(url));
  }

  public updateVideo(videoId: number, body: IUpdateVideoBody): Promise<IVideo> {
    const url = `${this._endpoint}/${videoId}`;
    return firstValueFrom(this._http.patch<IVideo>(url, body));
  }

  public deleteVideo(videoId: number): Promise<void> {
    const url = `${this._endpoint}/${videoId}`;
    return firstValueFrom(this._http.delete<void>(url));
  }

  public compileVideo(videoId: number): Promise<void> {
    const url = `${this._endpoint}/${videoId}/compile`;
    return firstValueFrom(this._http.post<void>(url, {}));
  }
}

export interface ICreateVideoBody {
  channelId: number;
  width: number;
  height: number;
  duration: number;
  reuseScenesAfterDays: number;
  reuseThumbnailAfterDays: number;
}

export interface IUpdateVideoBody {
  publishedAt?: string;
}
