import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IDashboardResponse } from '@vps/models';
import { firstValueFrom } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class DashboardProvider {
  private _endpoint: string = '/dashboard';

  constructor(private _http: HttpClient) {}

  public getDashboard(): Promise<IDashboardResponse> {
    const url = `${this._endpoint}`;
    return firstValueFrom(this._http.get<IDashboardResponse>(url));
  }
}
