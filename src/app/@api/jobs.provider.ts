import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IApiQuery, IJob, IPaginatedResponse } from '@vps/models';
import { buildApiQuery } from '@vps/utils';
import { firstValueFrom } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class JobsProvider {
  private _endpoint: string = '/jobs';

  constructor(private _http: HttpClient) {}

  public getJobs(params: IApiQuery): Promise<IPaginatedResponse<IJob>> {
    const query = buildApiQuery(params);
    const url = `${this._endpoint}${query}`;
    return firstValueFrom(this._http.get<IPaginatedResponse<IJob>>(url));
  }

  public getJob(jobId: number): Promise<IJob> {
    const url = `${this._endpoint}/${jobId}`;
    return firstValueFrom(this._http.get<IJob>(url));
  }

  public retryJob(jobId: number): Promise<IJob> {
    const url = `${this._endpoint}/${jobId}/retry`;
    return firstValueFrom(this._http.post<IJob>(url, {}));
  }
}
