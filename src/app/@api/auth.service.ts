import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AppCache } from '@vps/cache';
import { IAuthLogin } from '@vps/models';
import { AppStore } from '@vps/store';
import { AuthProvider } from './auth.provider';

@Injectable({ providedIn: 'root' })
export class AuthService {
  constructor(
    private _authProvider: AuthProvider,
    private _router: Router,
    private _store: AppStore
  ) {}

  public async login(data: IAuthLogin): Promise<void> {
    const { token } = await this._authProvider.login(data);
    AppCache.setCache('vps-ui.token', token);
    this._store.patchState({ authToken: token });
    await this._router.navigate(['/']);
  }

  public async getProfile(): Promise<void> {
    const profile = await this._authProvider.getProfile();
    this._store.patchState({ profile });
  }

  public logout(): void {
    AppCache.setCache('vps-ui.token', '');
    this._store.resetState();
    setTimeout(() => this._router.navigate(['/auth']));
  }
}
