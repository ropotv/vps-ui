import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IApiQuery, IPaginatedResponse, IThumbnail } from '@vps/models';
import { buildApiQuery } from '@vps/utils';
import { firstValueFrom } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ThumbnailsProvider {
  private _endpoint: string = '/thumbnails';

  constructor(private _http: HttpClient) {}

  public createThumbnails(files: File[]): Promise<void> {
    const url = `${this._endpoint}`;
    const formData = new FormData();
    for (const file of files) {
      formData.append('thumbnail', file);
    }
    return firstValueFrom(this._http.post<void>(url, formData));
  }

  public getThumbnails(
    params: IApiQuery
  ): Promise<IPaginatedResponse<IThumbnail>> {
    const query = buildApiQuery(params);
    const url = `${this._endpoint}${query}`;
    return firstValueFrom(this._http.get<IPaginatedResponse<IThumbnail>>(url));
  }

  public getThumbnail(thumbnailId: number): Promise<IThumbnail> {
    const url = `${this._endpoint}/${thumbnailId}`;
    return firstValueFrom(this._http.get<IThumbnail>(url));
  }

  public deleteThumbnail(thumbnailId: number): Promise<void> {
    const url = `${this._endpoint}/${thumbnailId}`;
    return firstValueFrom(this._http.delete<void>(url));
  }
}
