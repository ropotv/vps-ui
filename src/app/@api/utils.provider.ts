import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class UtilsProvider {
  private _endpoint: string = '/utils';

  constructor(private _http: HttpClient) {}

  public downloadBlob(data: IDownloadBody): Promise<Blob> {
    const url = `${this._endpoint}/download`;
    return firstValueFrom(
      this._http.post<any>(url, data, { responseType: 'blob' as any })
    );
  }
}

export interface IDownloadBody {
  url: string;
}
