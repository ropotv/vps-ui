import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IApiQuery, IJob, IPaginatedResponse, IResource } from '@vps/models';
import { buildApiQuery } from '@vps/utils';
import { firstValueFrom } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ResourcesProvider {
  private _endpoint: string = '/resources';

  constructor(private _http: HttpClient) {}

  public getResources(
    params: IApiQuery
  ): Promise<IPaginatedResponse<IResource>> {
    const query = buildApiQuery(params);
    const url = `${this._endpoint}${query}`;
    return firstValueFrom(this._http.get<IPaginatedResponse<IResource>>(url));
  }

  public getResource(resourceId: number): Promise<IResource> {
    const url = `${this._endpoint}/${resourceId}`;
    return firstValueFrom(this._http.get<IResource>(url));
  }

  public createResource(videoFile: File): Promise<IResource> {
    const url = `${this._endpoint}`;
    const formData = new FormData();
    formData.append('video', videoFile);
    return firstValueFrom(this._http.post<IResource>(url, formData));
  }

  public deleteResource(resourceId: number): Promise<void> {
    const url = `${this._endpoint}/${resourceId}`;
    return firstValueFrom(this._http.delete<void>(url));
  }

  public extractScenes(resourceId: number): Promise<IJob> {
    const url = `${this._endpoint}/${resourceId}/scenes/extract`;
    return firstValueFrom(this._http.post<IJob>(url, {}));
  }

  public compileScenes(
    resourceId: number,
    body: ICompileResourceScenesBody
  ): Promise<IJob> {
    const url = `${this._endpoint}/${resourceId}/scenes/compile`;
    return firstValueFrom(this._http.post<IJob>(url, body));
  }
}

export interface ICompileResourceScenesBody {
  sceneIds: number[];
}
