import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IApiQuery, IPaginatedResponse } from '@vps/models';
import { buildApiQuery } from '@vps/utils';
import { firstValueFrom } from 'rxjs';

import { IScene } from '@vps/models';

@Injectable({ providedIn: 'root' })
export class ScenesProvider {
  private _endpoint: string = '/scenes';

  constructor(private _http: HttpClient) {}

  public getScenes(params: IApiQuery): Promise<IPaginatedResponse<IScene>> {
    const query = buildApiQuery(params);
    const url = `${this._endpoint}${query}`;
    return firstValueFrom(this._http.get<IPaginatedResponse<IScene>>(url));
  }

  public getScene(sceneId: number): Promise<IScene> {
    const url = `${this._endpoint}/${sceneId}`;
    return firstValueFrom(this._http.get<IScene>(url));
  }

  public updateScene(sceneId: number, body: IUpdateSceneBody): Promise<IScene> {
    const url = `${this._endpoint}/${sceneId}`;
    return firstValueFrom(this._http.patch<IScene>(url, body));
  }

  public deleteScene(sceneId: number): Promise<void> {
    const url = `${this._endpoint}/${sceneId}`;
    return firstValueFrom(this._http.delete<void>(url));
  }
}

export interface IUpdateSceneBody {
  isVerified: boolean;
}
