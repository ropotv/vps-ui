type CacheKey = 'vps-ui.token' | 'vps-ui.sidebar-opened';

export class AppCache {
  static setCache(key: CacheKey, value: string): void {
    return localStorage.setItem(key, value);
  }

  static getCache(key: CacheKey): string | null {
    return localStorage.getItem(key);
  }
}
