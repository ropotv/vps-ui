import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthComponent } from './auth.component';
import { AuthGuard } from './auth.guard';
import { AuthRoutes } from './auth.routes';

@NgModule({
  imports: [CommonModule, RouterModule.forChild(AuthRoutes)],
  declarations: [AuthComponent],
  providers: [AuthGuard],
})
export class AuthModule {}
