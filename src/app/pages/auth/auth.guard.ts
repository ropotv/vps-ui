import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AppStore } from '@vps/store';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private _store: AppStore, private _router: Router) {}

  public canActivate(): boolean {
    if (!this._store.get.authToken) {
      return true;
    }

    this._router.navigate(['/']);
    return false;
  }
}
