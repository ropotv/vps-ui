import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { AuthService } from '@vps/api';
import { createLoginForm } from './login.form';

@Component({
  templateUrl: './login.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
  ],
})
export class LoginComponent {
  constructor(private _authService: AuthService) {}

  public form = createLoginForm();

  public async onSubmit(): Promise<void> {
    if (this.form.valid) {
      await this._authService.login({
        email: this.form.value.email!,
        password: this.form.value.password!,
      });
    }
  }
}
