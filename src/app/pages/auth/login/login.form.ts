import { inject } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

export type ILoginFormData = FormGroup<{
  email: FormControl<string>;
  password: FormControl<string>;
}>;

export const createLoginForm = (): ILoginFormData => {
  const { nonNullable } = inject(FormBuilder);

  return nonNullable.group({
    email: nonNullable.control<string>('', [Validators.required]),
    password: nonNullable.control<string>('', [Validators.required]),
  });
};
