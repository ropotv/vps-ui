import { Route } from '@angular/router';
import { AuthComponent } from './auth.component';
import { AuthGuard } from './auth.guard';

export const AuthRoutes: Route[] = [
  {
    path: '',
    component: AuthComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
      },
      {
        path: 'login',
        loadComponent: () =>
          import('./login/login.component').then((m) => m.LoginComponent),
      },
    ],
  },
];
