import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatRippleModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { RouterModule } from '@angular/router';
import { AvatarComponent } from '@vps/components';
import { MainSidebarComponent } from './@components/main-sidebar';
import { MainComponent } from './main.component';
import { MainGuard } from './main.guard';
import { MainRoutes } from './main.routes';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(MainRoutes),
    MatButtonModule,
    MatIconModule,
    MatSidenavModule,
    MatRippleModule,
    MatListModule,
    AvatarComponent,
  ],
  declarations: [MainComponent, MainSidebarComponent],
  providers: [MainGuard],
})
export class MainModule {}
