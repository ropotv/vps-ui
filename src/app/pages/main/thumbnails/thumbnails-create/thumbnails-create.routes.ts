import { Route } from '@angular/router';
import { ThumbnailsCreateComponent } from './thumbnails-create.component';

export const ThumbnailsCreateRoutes: Route[] = [
  {
    path: '',
    component: ThumbnailsCreateComponent,
  },
];
