import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ThumbnailsProvider } from '@vps/api';
import { DialogLayoutComponent } from '@vps/components';
import { getEventDetector } from '@vps/utils';

@Component({
  templateUrl: './thumbnails-create.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ThumbnailsCreateComponent {
  constructor(
    private _thumbnailsProvider: ThumbnailsProvider,
    private _snackBar: MatSnackBar
  ) {}

  @ViewChild('dialogLayout')
  private _dialogLayout: DialogLayoutComponent | undefined;

  public eventDetector = getEventDetector('refresh-thumbnail-list');
  public isUploading = false;

  public onSubmit(filePicker: HTMLInputElement): void {
    const videoFiles = Array.from(filePicker.files!);
    if (!videoFiles?.length) {
      this._snackBar.open('Please select thumbnails to continue');
      return;
    }

    this.isUploading = true;
    this._thumbnailsProvider
      .createThumbnails(videoFiles)
      .then(() => {
        this.eventDetector.markAsChanged();
        this._snackBar.open('Thumbnails were uploaded successfully!');
        this._dialogLayout?.close();
      })
      .catch(() => (this.isUploading = false));
  }
}
