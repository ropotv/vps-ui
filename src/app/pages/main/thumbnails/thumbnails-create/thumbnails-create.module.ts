import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { RouterModule } from '@angular/router';
import { DialogLayoutComponent } from '@vps/components';
import { ThumbnailsCreateComponent } from './thumbnails-create.component';
import { ThumbnailsCreateRoutes } from './thumbnails-create.routes';

@NgModule({
  imports: [
    RouterModule.forChild(ThumbnailsCreateRoutes),
    CommonModule,
    MatButtonModule,
    DialogLayoutComponent,
    MatProgressSpinnerModule,
  ],
  declarations: [ThumbnailsCreateComponent],
})
export class ThumbnailsCreateModule {}
