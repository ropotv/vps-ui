import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule } from '@angular/router';
import { SceneMenuComponent, ThumbnailMenuComponent } from '@vps/menus';
import {
  FormatBytesPipe,
  FormatMillisecondsPipe,
  StorageFileUrlPipe,
} from '@vps/pipes';
import { ThumbnailsComponent } from './thumbnails.component';
import { ThumbnailsRoutes } from './thumbnails.routes';

@NgModule({
  imports: [
    RouterModule.forChild(ThumbnailsRoutes),
    CommonModule,
    MatPaginatorModule,
    StorageFileUrlPipe,
    FormatBytesPipe,
    FormatMillisecondsPipe,
    SceneMenuComponent,
    ThumbnailMenuComponent,
    MatButtonModule,
    MatTooltipModule,
    MatIconModule,
  ],
  declarations: [ThumbnailsComponent],
})
export class ThumbnailsModule {}
