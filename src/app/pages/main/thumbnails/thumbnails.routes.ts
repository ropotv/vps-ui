import { Route } from '@angular/router';
import { ThumbnailsComponent } from './thumbnails.component';

export const ThumbnailsRoutes: Route[] = [
  {
    path: '',
    component: ThumbnailsComponent,
    children: [
      {
        path: 'create',
        loadChildren: () =>
          import('./thumbnails-create/thumbnails-create.module').then(
            (m) => m.ThumbnailsCreateModule
          ),
      },
    ],
  },
];
