import { ChangeDetectionStrategy, Component } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { ThumbnailsProvider } from '@vps/api';
import { AppEvents } from '@vps/events';
import { BehaviorSubject, startWith, switchMap } from 'rxjs';

@Component({
  templateUrl: './thumbnails.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ThumbnailsComponent {
  constructor(
    private _thumbnailsProvider: ThumbnailsProvider,
    private _events: AppEvents
  ) {}

  public pagination$ = new BehaviorSubject<PageEvent>({
    pageSize: 10,
    pageIndex: 0,
    length: 0,
  });
  public thumbnails$ = this.pagination$.pipe(
    switchMap((pagination) =>
      this._events.on('refresh-thumbnail-list').pipe(
        startWith(''),
        switchMap(() =>
          this._thumbnailsProvider.getThumbnails({
            order: 'thumbnail.createdAt~DESC~AND',
            size: pagination.pageSize,
            page: pagination.pageIndex,
          })
        )
      )
    )
  );

  public syncScenes(): void {
    this._events.publish('refresh-thumbnail-list');
  }
}
