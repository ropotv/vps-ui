import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AuthService } from '@vps/api';
import { AppCache } from '@vps/cache';
import { AlertService } from '@vps/features';
import { AppStore } from '@vps/store';
import { map } from 'rxjs';

@Component({
  templateUrl: './main.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MainComponent {
  constructor(
    private _store: AppStore,
    private _authService: AuthService,
    private _alert: AlertService
  ) {}

  public userProfile$ = this._store.state$.pipe(map((s) => s.profile));

  public get sidebarOpened(): boolean {
    const cached = AppCache.getCache('vps-ui.sidebar-opened');
    if (!cached) return true;
    return cached === 'true';
  }

  public set sidebarOpened(value: boolean) {
    AppCache.setCache('vps-ui.sidebar-opened', `${value}`);
  }

  public onLogout(): void {
    this._alert
      .confirm({
        caption: 'Logout from system',
        description:
          'Are you sure you want to logout? You will need to login again to access this page.',
        acceptBtn: 'Yes, logout',
      })
      .subscribe(() => this._authService.logout());
  }
}
