import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { IUserProfile } from '@vps/models';

@Component({
  selector: 'app-main-sidebar',
  templateUrl: './main-sidebar.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MainSidebarComponent {
  @Input()
  public userProfile: IUserProfile | null | undefined;

  @Output()
  public onLogout = new EventEmitter<void>();

  public menus = [
    {
      caption: 'Library',
      description: 'Manage your resources',
      items: [
        {
          label: 'Overview',
          routerLink: '/overview',
          icon: 'space_dashboard',
        },
        {
          label: 'Jobs',
          routerLink: '/jobs',
          icon: 'memory',
        },
        {
          label: 'Resources',
          routerLink: '/resources',
          icon: 'dvr',
        },
        {
          label: 'Scenes',
          routerLink: '/scenes',
          icon: 'video_library',
        },
        {
          label: 'Thumbnails',
          routerLink: '/thumbnails',
          icon: 'photo_library',
        },
      ],
    },
    {
      caption: 'Channels',
      description: 'All your YouTube channels',
      items: [
        {
          label: 'Manage Channels',
          routerLink: '/channels',
          icon: 'subscriptions',
        },
      ],
    },
  ];
}
