import { Route } from '@angular/router';
import { MainComponent } from './main.component';
import { MainGuard } from './main.guard';

export const MainRoutes: Route[] = [
  {
    path: '',
    component: MainComponent,
    canActivate: [MainGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: '/overview',
      },
      {
        path: 'overview',
        loadChildren: () =>
          import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
      },
      {
        path: 'jobs',
        loadChildren: () =>
          import('./jobs/jobs.module').then((m) => m.JobsModule),
      },
      {
        path: 'resources',
        loadChildren: () =>
          import('./resources/resources.module').then((m) => m.ResourcesModule),
      },
      {
        path: 'scenes',
        loadChildren: () =>
          import('./scenes/scenes.module').then((m) => m.ScenesModule),
      },
      {
        path: 'thumbnails',
        loadChildren: () =>
          import('./thumbnails/thumbnails.module').then(
            (m) => m.ThumbnailsModule
          ),
      },
      {
        path: 'channels',
        loadChildren: () =>
          import('./channels/channels.module').then((m) => m.ChannelsModule),
      },
    ],
  },
];
