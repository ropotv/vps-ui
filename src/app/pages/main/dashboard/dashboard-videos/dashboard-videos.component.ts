import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-dashboard-videos',
  templateUrl: './dashboard-videos.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardVideosComponent {
  @Input()
  public amount: number | undefined;

  @Input()
  public size: number | undefined;

  @Input()
  public duration: number | undefined;
}
