import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-dashboard-resources',
  templateUrl: './dashboard-resources.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardResourcesComponent {
  @Input()
  public amount: number | undefined;

  @Input()
  public size: number | undefined;

  @Input()
  public processedPercentage: number | undefined;
}
