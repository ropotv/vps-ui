import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-dashboard-scenes',
  templateUrl: './dashboard-scenes.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardScenesComponent {
  @Input()
  public verifiedPercentage: number | undefined;

  @Input()
  public usedPercentage: number | undefined;

  @Input()
  public amount: number | undefined;

  @Input()
  public size: number | undefined;

  @Input()
  public duration: number | undefined;
}
