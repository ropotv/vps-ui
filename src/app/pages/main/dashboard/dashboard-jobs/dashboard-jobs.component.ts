import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-dashboard-jobs',
  templateUrl: './dashboard-jobs.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardJobsComponent {
  @Input()
  public processing: number | undefined;

  @Input()
  public queued: number | undefined;

  @Input()
  public failed: number | undefined;

  @Input()
  public done: number | undefined;
}
