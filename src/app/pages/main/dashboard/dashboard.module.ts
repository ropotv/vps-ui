import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormatBytesPipe, FormatMillisecondsPipe } from '@vps/pipes';
import { DashboardJobsComponent } from './dashboard-jobs';
import { DashboardResourcesComponent } from './dashboard-resources';
import { DashboardScenesComponent } from './dashboard-scenes';
import { DashboardVideosComponent } from './dashboard-videos';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutes } from './dashboard.routes';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DashboardRoutes),
    FormatBytesPipe,
    FormatMillisecondsPipe,
  ],
  declarations: [
    DashboardComponent,
    DashboardScenesComponent,
    DashboardResourcesComponent,
    DashboardVideosComponent,
    DashboardJobsComponent,
  ],
})
export class DashboardModule {}
