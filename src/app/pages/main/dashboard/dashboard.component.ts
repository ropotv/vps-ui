import { ChangeDetectionStrategy, Component } from '@angular/core';
import { DashboardProvider } from '@vps/api';

@Component({
  templateUrl: './dashboard.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardComponent {
  constructor(private _dashboardProvider: DashboardProvider) {}

  public dashboard$ = this._dashboardProvider.getDashboard();
}
