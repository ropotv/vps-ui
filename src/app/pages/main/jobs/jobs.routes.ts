import { Route } from '@angular/router';
import { JobsComponent } from './jobs.component';

export const JobsRoutes: Route[] = [
  {
    path: '',
    component: JobsComponent,
    children: [
      {
        path: ':jobId',
        loadChildren: () =>
          import('./job-details/job-details.module').then(
            (m) => m.JobDetailsModule
          ),
      },
    ],
  },
];
