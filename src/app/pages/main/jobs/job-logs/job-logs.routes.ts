import { Route } from '@angular/router';
import { JobLogsComponent } from './job-logs.component';

export const JobLogsRoutes: Route[] = [
  {
    path: '',
    component: JobLogsComponent,
  },
];
