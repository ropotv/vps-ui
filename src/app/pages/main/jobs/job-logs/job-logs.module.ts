import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { DialogLayoutComponent } from '@vps/components';
import { JobLogColorPipe, JobLogIconPipe } from '@vps/pipes';
import { JobLogsComponent } from './job-logs.component';
import { JobLogsRoutes } from './job-logs.routes';

@NgModule({
  imports: [
    RouterModule.forChild(JobLogsRoutes),
    CommonModule,
    DialogLayoutComponent,
    MatIconModule,
    JobLogColorPipe,
    JobLogIconPipe,
  ],
  declarations: [JobLogsComponent],
})
export class JobLogsModule {}
