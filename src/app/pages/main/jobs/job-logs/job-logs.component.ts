import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JobsProvider } from '@vps/api';
import { IJobLog } from '@vps/models';
import { filter, map, switchMap } from 'rxjs';

@Component({
  templateUrl: './job-logs.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JobLogsComponent {
  constructor(
    private _jobsProvider: JobsProvider,
    private _route: ActivatedRoute
  ) {}

  public sortLogFn = (a: IJobLog, b: IJobLog) =>
    new Date(a.createdAt) > new Date(b.createdAt) ? -1 : 1;

  public job$ = this._route.parent!.parent!.paramMap.pipe(
    map((paramMap) => paramMap.get('jobId')),
    filter(Boolean),
    switchMap((jobId) => this._jobsProvider.getJob(Number(jobId)))
  );
}
