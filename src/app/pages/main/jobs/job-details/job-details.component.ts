import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JobsProvider } from '@vps/api';
import { filter, map, switchMap } from 'rxjs';

@Component({
  templateUrl: './job-details.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JobDetailsComponent {
  constructor(
    private _jobsProvider: JobsProvider,
    private _route: ActivatedRoute
  ) {}

  public job$ = this._route.paramMap.pipe(
    map((paramMap) => paramMap.get('jobId')),
    filter(Boolean),
    switchMap((jobId) => this._jobsProvider.getJob(Number(jobId)))
  );
}
