import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { DialogLayoutComponent } from '@vps/components';
import {
  FormatMillisecondsPipe,
  JobDurationPipe,
  JobStatusBgPipe,
  JobTypePipe,
} from '@vps/pipes';
import { JobDetailsComponent } from './job-details.component';
import { JobDetailsRoutes } from './job-details.routes';

@NgModule({
  imports: [
    RouterModule.forChild(JobDetailsRoutes),
    CommonModule,
    MatIconModule,
    MatButtonModule,
    DialogLayoutComponent,
    JobDurationPipe,
    JobStatusBgPipe,
    JobTypePipe,
    FormatMillisecondsPipe,
  ],
  declarations: [JobDetailsComponent],
})
export class JobDetailsModule {}
