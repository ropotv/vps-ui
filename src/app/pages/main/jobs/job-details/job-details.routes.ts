import { Route } from '@angular/router';
import { JobDetailsComponent } from './job-details.component';

export const JobDetailsRoutes: Route[] = [
  {
    path: '',
    component: JobDetailsComponent,
    children: [
      {
        path: 'logs',
        loadChildren: () =>
          import('../job-logs/job-logs.module').then((m) => m.JobLogsModule),
      },
    ],
  },
];
