import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { RouterModule } from '@angular/router';
import { JobMenuComponent } from '@vps/menus';
import {
  FormatMillisecondsPipe,
  JobDurationPipe,
  JobStatusBgPipe,
  JobTypePipe,
} from '@vps/pipes';
import { JobsComponent } from './jobs.component';
import { JobsRoutes } from './jobs.routes';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(JobsRoutes),
    DragDropModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    JobDurationPipe,
    JobStatusBgPipe,
    JobTypePipe,
    JobMenuComponent,
    FormatMillisecondsPipe,
  ],
  declarations: [JobsComponent],
})
export class JobsModule {}
