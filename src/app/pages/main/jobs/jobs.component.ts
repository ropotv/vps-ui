import { ChangeDetectionStrategy, Component } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { JobsProvider } from '@vps/api';
import { AppEvents } from '@vps/events';
import { BehaviorSubject, startWith, switchMap } from 'rxjs';
import { createJobsFilter } from './jobs.filter';

@Component({
  templateUrl: './jobs.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JobsComponent {
  constructor(
    private _jobsProvider: JobsProvider,
    private _snackBar: MatSnackBar,
    private _route: ActivatedRoute,
    private _events: AppEvents
  ) {}

  public columns = [
    'id',
    'type',
    'startedAt',
    'duration',
    'status',
    'createdAt',
    'actions',
  ];

  public pagination$ = new BehaviorSubject<PageEvent>({
    pageSize: 10,
    pageIndex: 0,
    length: 0,
  });
  public jobs$ = this._route.queryParams.pipe(
    switchMap((queryParams) =>
      this.pagination$.pipe(
        switchMap((pagination) =>
          this._events.on('refresh-job-list').pipe(
            startWith(''),
            switchMap(() =>
              this._jobsProvider.getJobs({
                filter: createJobsFilter(queryParams),
                order: 'job.createdAt~DESC~AND',
                size: pagination.pageSize,
                page: pagination.pageIndex,
              })
            )
          )
        )
      )
    )
  );

  public syncJobs(): void {
    this._events.publish('refresh-job-list');
  }
}
