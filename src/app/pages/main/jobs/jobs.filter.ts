export function createJobsFilter(params: Record<string, string>): string {
  const { compileResourceId, extractResourceId, videoId } = params;

  let filter = '';

  if (compileResourceId) {
    filter += `compileScenesJobResource.id~eq~${compileResourceId}~OR;`;
  }
  if (extractResourceId) {
    filter += `extractResourceJobResource.id~eq~${extractResourceId}~OR;`;
  }
  if (videoId) {
    filter += `compileVideoJobVideo.id~eq~${videoId}~OR;`;
  }

  return filter.slice(0, filter.lastIndexOf(';'));
}
