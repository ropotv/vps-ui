import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { RouterModule } from '@angular/router';
import { SceneMenuComponent } from '@vps/menus';
import {
  FormatBytesPipe,
  FormatMillisecondsPipe,
  StorageFileUrlPipe,
} from '@vps/pipes';
import { ScenesComponent } from './scenes.component';
import { ScenesRoutes } from './scenes.routes';

@NgModule({
  imports: [
    RouterModule.forChild(ScenesRoutes),
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    StorageFileUrlPipe,
    FormatBytesPipe,
    FormatMillisecondsPipe,
    SceneMenuComponent,
  ],
  declarations: [ScenesComponent],
})
export class ScenesModule {}
