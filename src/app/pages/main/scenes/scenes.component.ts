import { ChangeDetectionStrategy, Component } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { ScenesProvider } from '@vps/api';
import { AppEvents } from '@vps/events';
import { BehaviorSubject, startWith, switchMap } from 'rxjs';
import { createScenesFilter } from './scenes.filter';

@Component({
  templateUrl: './scenes.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScenesComponent {
  constructor(
    private _scenesProvider: ScenesProvider,
    private _route: ActivatedRoute,
    private _events: AppEvents
  ) {}

  public columns = ['image', 'size', 'verifiedAt', 'createdAt', 'actions'];

  public pagination$ = new BehaviorSubject<PageEvent>({
    pageSize: 10,
    pageIndex: 0,
    length: 0,
  });
  public scenes$ = this._route.queryParams.pipe(
    switchMap((queryParams) =>
      this.pagination$.pipe(
        switchMap((pagination) =>
          this._events.on('refresh-scene-list').pipe(
            startWith(''),
            switchMap(() =>
              this._scenesProvider.getScenes({
                filter: createScenesFilter(queryParams),
                order: 'scene.createdAt~DESC~AND',
                size: pagination.pageSize,
                page: pagination.pageIndex,
              })
            )
          )
        )
      )
    )
  );

  public syncScenes(): void {
    this._events.publish('refresh-scene-list');
  }
}
