import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { DialogLayoutComponent, VideoPlayerComponent } from '@vps/components';
import { SceneMenuComponent } from '@vps/menus';
import { FormatBytesPipe, StorageFileUrlPipe } from '@vps/pipes';
import { SceneDetailsComponent } from './scene-details.component';
import { SceneDetailsRoutes } from './scene-details.routes';

@NgModule({
  imports: [
    RouterModule.forChild(SceneDetailsRoutes),
    CommonModule,
    MatDividerModule,
    MatButtonModule,
    MatIconModule,
    DialogLayoutComponent,
    StorageFileUrlPipe,
    FormatBytesPipe,
    VideoPlayerComponent,
    SceneMenuComponent,
  ],
  declarations: [SceneDetailsComponent],
})
export class SceneDetailsModule {}
