import { Route } from '@angular/router';
import { SceneDetailsComponent } from './scene-details.component';

export const SceneDetailsRoutes: Route[] = [
  {
    path: '',
    component: SceneDetailsComponent,
  },
];
