import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ScenesProvider } from '@vps/api';
import { DialogLayoutComponent } from '@vps/components';
import { AppEvents } from '@vps/events';
import { getEventDetector } from '@vps/utils';
import { filter, map, startWith, switchMap } from 'rxjs';

@Component({
  templateUrl: './scene-details.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SceneDetailsComponent {
  constructor(
    private _scenesProvider: ScenesProvider,
    private _route: ActivatedRoute,
    private _events: AppEvents
  ) {}

  @ViewChild('dialogLayout')
  private _dialogLayout: DialogLayoutComponent | undefined;

  public eventDetector = getEventDetector('refresh-scene-list');

  public scene$ = this._route.paramMap.pipe(
    map((paramMap) => paramMap.get('sceneId')),
    filter(Boolean),
    switchMap((sceneId) =>
      this._events.on('refresh-scene-details').pipe(
        startWith(''),
        switchMap(() => this._scenesProvider.getScene(Number(sceneId)))
      )
    )
  );

  public markAsChanged(): void {
    this._events.publish('refresh-scene-details');
    this.eventDetector.markAsChanged();
  }

  public closeDialog(): void {
    this.eventDetector.markAsChanged();
    this._dialogLayout?.close();
  }
}
