export function createScenesFilter(params: Record<string, string>): string {
  const { resourceId, videoId } = params;
  let filter = '';

  if (resourceId) {
    filter += `resource.id~eq~${resourceId}~OR`;
  }
  if (videoId) {
    filter += `videos.id~eq~${videoId}~OR`;
  }

  return filter.slice(0, filter.lastIndexOf(';'));
}
