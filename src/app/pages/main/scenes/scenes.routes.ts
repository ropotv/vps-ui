import { Route } from '@angular/router';
import { ScenesComponent } from './scenes.component';

export const ScenesRoutes: Route[] = [
  {
    path: '',
    component: ScenesComponent,
    children: [
      {
        path: ':sceneId',
        loadChildren: () =>
          import('./scene-details/scene-details.module').then(
            (m) => m.SceneDetailsModule
          ),
      },
    ],
  },
];
