import { Route } from '@angular/router';
import { ChannelDetailsComponent } from './channel-details.component';

export const ChannelDetailsRoutes: Route[] = [
  {
    path: '',
    component: ChannelDetailsComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'videos',
      },
      {
        path: 'videos',
        loadChildren: () =>
          import('../channel-video-list/channel-video-list.module').then(
            (m) => m.ChannelVideoListModule
          ),
      },
    ],
  },
];
