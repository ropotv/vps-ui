import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChannelsProvider, VideosProvider } from '@vps/api';
import { map, switchMap } from 'rxjs';

@Component({
  templateUrl: './channel-details.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChannelDetailsComponent {
  constructor(
    private _videosProvider: VideosProvider,
    private _channelsProvider: ChannelsProvider,
    private _route: ActivatedRoute,
    private _router: Router
  ) {}

  public channel$ = this._route.paramMap.pipe(
    map((paramMap) => paramMap.get('channelId')),
    switchMap((channelId) =>
      this._channelsProvider.getChannel(Number(channelId))
    )
  );
  public publishedVideosAmount$ = this.channel$.pipe(
    switchMap((channel) =>
      this._videosProvider.countVideos({
        filter: `channel.id~eq~${channel.id}~AND;video.publishedAt~notNull~true~AND`,
      })
    )
  );

  public syncChannels(): void {
    this._router
      .navigate(['/'])
      .then(() => this._router.navigate(['/channels']));
  }
}
