import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule } from '@angular/router';
import { AvatarComponent } from '@vps/components';
import { ChannelMenuComponent } from '@vps/menus';
import { ChannelDetailsComponent } from './channel-details.component';
import { ChannelDetailsRoutes } from './channel-details.routes';

@NgModule({
  imports: [
    RouterModule.forChild(ChannelDetailsRoutes),
    CommonModule,
    AvatarComponent,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    MatDividerModule,
    ChannelMenuComponent,
  ],
  declarations: [ChannelDetailsComponent],
})
export class ChannelDetailsModule {}
