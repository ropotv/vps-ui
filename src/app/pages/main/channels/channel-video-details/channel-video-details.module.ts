import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DialogLayoutComponent, VideoPlayerComponent } from '@vps/components';
import { VideoMenuComponent } from '@vps/menus';
import {
  StorageFileUrlPipe,
  VideoStatusBgPipe,
  VideoStatusTextPipe,
} from '@vps/pipes';
import { ChannelVideoDetailsComponent } from './channel-video-details.component';
import { ChannelVideoDetailsRoutes } from './channel-video-details.routes';

@NgModule({
  imports: [
    RouterModule.forChild(ChannelVideoDetailsRoutes),
    CommonModule,
    DialogLayoutComponent,
    VideoPlayerComponent,
    VideoMenuComponent,
    VideoStatusBgPipe,
    VideoStatusTextPipe,
    StorageFileUrlPipe,
  ],
  declarations: [ChannelVideoDetailsComponent],
})
export class ChannelVideoDetailsModule {}
