import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VideosProvider } from '@vps/api';
import { DialogLayoutComponent } from '@vps/components';
import { AppEvents } from '@vps/events';
import { getEventDetector } from '@vps/utils';
import { map, startWith, switchMap } from 'rxjs';

@Component({
  templateUrl: './channel-video-details.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChannelVideoDetailsComponent {
  constructor(
    private _route: ActivatedRoute,
    private _videosProvider: VideosProvider,
    private _events: AppEvents
  ) {}

  @ViewChild('dialogLayout')
  private _dialogLayout: DialogLayoutComponent | undefined;

  public eventDetector = getEventDetector('refresh-channel-video-list');

  public video$ = this._route.paramMap.pipe(
    map((paramMap) => paramMap.get('videoId')),
    switchMap((videoId) =>
      this._events.on('refresh-channel-video-details').pipe(
        startWith(''),
        switchMap(() => this._videosProvider.getVideo(Number(videoId)))
      )
    )
  );

  public markAsChanged() {
    this._events.publish('refresh-channel-video-details');
    this.eventDetector.markAsChanged();
  }

  public closeDialog() {
    this.eventDetector.markAsChanged();
    this._dialogLayout?.close();
  }
}
