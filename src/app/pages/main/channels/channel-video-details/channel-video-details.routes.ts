import { Route } from '@angular/router';
import { ChannelVideoDetailsComponent } from './channel-video-details.component';

export const ChannelVideoDetailsRoutes: Route[] = [
  {
    path: '',
    component: ChannelVideoDetailsComponent,
  },
];
