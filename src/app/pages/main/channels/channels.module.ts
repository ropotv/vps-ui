import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule } from '@angular/router';
import { AvatarComponent } from '@vps/components';
import { ChannelsComponent } from './channels.component';
import { ChannelsRoutes } from './channels.routes';

@NgModule({
  imports: [
    RouterModule.forChild(ChannelsRoutes),
    CommonModule,
    AvatarComponent,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    ReactiveFormsModule,
    DragDropModule,
  ],
  declarations: [ChannelsComponent],
})
export class ChannelsModule {}
