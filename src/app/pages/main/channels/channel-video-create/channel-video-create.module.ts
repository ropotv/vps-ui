import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { RouterModule } from '@angular/router';
import { DialogLayoutComponent } from '@vps/components';
import { ChannelVideoCreateComponent } from './channel-video-create.component';
import { ChannelVideoCreateRoutes } from './channel-video-create.routes';

@NgModule({
  declarations: [ChannelVideoCreateComponent],
  imports: [
    RouterModule.forChild(ChannelVideoCreateRoutes),
    CommonModule,
    DialogLayoutComponent,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonModule,
  ],
})
export class ChannelVideoCreateModule {}
