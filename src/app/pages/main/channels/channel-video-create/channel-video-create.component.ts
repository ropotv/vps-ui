import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { ChannelsProvider, VideosProvider } from '@vps/api';
import { DialogLayoutComponent } from '@vps/components';
import { AppEvents } from '@vps/events';
import { getEventDetector } from '@vps/utils';
import { getCreateVideoForm } from './channel-video-create.form';

@Component({
  templateUrl: './channel-video-create.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChannelVideoCreateComponent {
  constructor(
    private _channelsProvider: ChannelsProvider,
    private _videosProvider: VideosProvider,
    private _route: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private _events: AppEvents
  ) {}

  @ViewChild('dialogLayout')
  private _dialogLayout: DialogLayoutComponent | undefined;

  public eventDetector = getEventDetector('refresh-channel-video-list');
  public form = getCreateVideoForm();

  public async onSubmit(compile: boolean): Promise<void> {
    if (this.form.invalid) return;

    const channelId = Number(
      this._route.parent!.parent!.parent!.parent!.snapshot.paramMap.get(
        'channelId'
      )
    );
    const formValue = this.form.getRawValue();
    const video = await this._videosProvider.createVideo({
      channelId,
      width: 1920,
      height: 1080,
      duration: formValue.duration,
      reuseThumbnailAfterDays: formValue.reuseThumbnailAfterDays,
      reuseScenesAfterDays: formValue.reuseScenesAfterDays,
    });

    if (compile) {
      await this._videosProvider.compileVideo(video.id);
      this._snackBar.open('Video was created and scheduled for compilation!');
    } else {
      this._snackBar.open('Video was created successfully!');
    }

    this.eventDetector.markAsChanged();
    this._dialogLayout?.close();
  }
}
