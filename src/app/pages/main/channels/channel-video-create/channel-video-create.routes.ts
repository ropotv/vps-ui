import { Route } from '@angular/router';
import { ChannelVideoCreateComponent } from './channel-video-create.component';

export const ChannelVideoCreateRoutes: Route[] = [
  {
    path: '',
    component: ChannelVideoCreateComponent,
  },
];
