import { inject } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

export type IChannelVideoCreateForm = FormGroup<{
  duration: FormControl<number>;
  reuseScenesAfterDays: FormControl<number>;
  reuseThumbnailAfterDays: FormControl<number>;
}>;

export function getCreateVideoForm(): IChannelVideoCreateForm {
  const { nonNullable } = inject(FormBuilder);

  return nonNullable.group({
    duration: nonNullable.control(610, [
      Validators.required,
      Validators.min(1),
    ]),
    reuseScenesAfterDays: nonNullable.control(180, [
      Validators.required,
      Validators.min(0),
    ]),
    reuseThumbnailAfterDays: nonNullable.control(1095, [
      Validators.required,
      Validators.min(0),
    ]),
  });
}
