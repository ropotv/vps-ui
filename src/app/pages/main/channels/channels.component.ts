import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ChannelsProvider } from '@vps/api';
import { AppEvents } from '@vps/events';
import { AlertService } from '@vps/features';
import { IChannel } from '@vps/models';
import { startWith, switchMap } from 'rxjs';

@Component({
  templateUrl: './channels.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChannelsComponent {
  constructor(
    private _channelsProvider: ChannelsProvider,
    private _router: Router,
    private _route: ActivatedRoute,
    private _alert: AlertService,
    private _snackBar: MatSnackBar,
    private _events: AppEvents
  ) {}

  public createChannelForm = new FormGroup({ name: new FormControl('') });
  public channels$ = this._events.on('refresh-channel-list').pipe(
    startWith(''),
    switchMap(() =>
      this._channelsProvider
        .getChannels({ order: 'channel.priority~ASC~AND' })
        .then(([channels]) => {
          const firstChannelId = channels?.[0]?.id;

          const childRoute = this._route.children[0];
          const isDetails = childRoute?.snapshot.paramMap.has('channelId');

          if (firstChannelId && !isDetails) {
            this._router.navigate([firstChannelId], {
              relativeTo: this._route,
            });
          }

          return channels;
        })
    )
  );

  public async onChannelsReorder(
    channels: IChannel[],
    event: CdkDragDrop<number>
  ): Promise<void> {
    moveItemInArray(channels, event.previousIndex, event.currentIndex);
    const current = channels[event.currentIndex];
    const predecessor = channels[event.currentIndex - 1];
    const successor = channels[event.currentIndex + 1];
    await this._channelsProvider.reorderChannel(current.id, {
      predecessorPriority: predecessor?.priority,
      successorPriority: successor?.priority,
    });
    this._events.publish('refresh-channel-list');
  }

  public onChannelCreate(): void {
    const { name } = this.createChannelForm.value;
    if (!name) {
      this._snackBar.open('Please enter a name for chanel before creating it!');
      return;
    }

    this._alert
      .confirm({
        caption: 'Create Channel?',
        description: `Do you want to create a channel with name '${name}' ?`,
        acceptBtn: 'Create Channel',
      })
      .subscribe(async () => {
        await this._channelsProvider.createChannel({ name });
        this.createChannelForm.reset();
        this._snackBar.open('Channel was created successfully!');
        this._events.publish('refresh-channel-list');
      });
  }
}
