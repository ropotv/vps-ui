import { Route } from '@angular/router';
import { ChannelsComponent } from './channels.component';

export const ChannelsRoutes: Route[] = [
  {
    path: '',
    component: ChannelsComponent,
    children: [
      {
        path: ':channelId',
        loadChildren: () =>
          import('./channel-details/channel-details.module').then(
            (m) => m.ChannelDetailsModule
          ),
      },
    ],
  },
];
