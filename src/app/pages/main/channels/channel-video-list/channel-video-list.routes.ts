import { Route } from '@angular/router';
import { ChannelVideoListComponent } from './channel-video-list.component';

export const ChannelVideoListRoutes: Route[] = [
  {
    path: '',
    component: ChannelVideoListComponent,
    children: [
      {
        path: 'create',
        loadChildren: () =>
          import('../channel-video-create/channel-video-create.module').then(
            (m) => m.ChannelVideoCreateModule
          ),
      },
      {
        path: ':videoId',
        loadChildren: () =>
          import('../channel-video-details/channel-video-details.module').then(
            (m) => m.ChannelVideoDetailsModule
          ),
      },
    ],
  },
];
