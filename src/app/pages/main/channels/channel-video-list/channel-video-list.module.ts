import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule } from '@angular/router';
import { VideoMenuComponent } from '@vps/menus';
import {
  FormatMillisecondsPipe,
  StorageFileUrlPipe,
  VideoStatusBgPipe,
  VideoStatusTextPipe,
} from '@vps/pipes';
import { ChannelVideoListComponent } from './channel-video-list.component';
import { ChannelVideoListRoutes } from './channel-video-list.routes';

@NgModule({
  imports: [
    RouterModule.forChild(ChannelVideoListRoutes),
    CommonModule,
    MatPaginatorModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    VideoMenuComponent,
    VideoStatusBgPipe,
    VideoStatusTextPipe,
    StorageFileUrlPipe,
    FormatMillisecondsPipe,
  ],
  declarations: [ChannelVideoListComponent],
})
export class ChannelVideoListModule {}
