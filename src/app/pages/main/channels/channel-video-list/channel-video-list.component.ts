import { ChangeDetectionStrategy, Component } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { ChannelsProvider, VideosProvider } from '@vps/api';
import { AppEvents } from '@vps/events';
import { BehaviorSubject, map, startWith, switchMap, tap } from 'rxjs';

@Component({
  templateUrl: './channel-video-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChannelVideoListComponent {
  constructor(
    private _route: ActivatedRoute,
    private _channelsProvider: ChannelsProvider,
    private _videosProvider: VideosProvider,
    private _events: AppEvents
  ) {}

  public columns = ['video', 'status', 'actions'];

  public pagination$ = new BehaviorSubject<PageEvent>({
    pageSize: 10,
    pageIndex: 0,
    length: 0,
  });

  public channel$ = this._route.parent!.parent!.paramMap.pipe(
    map((paramMap) => paramMap.get('channelId')),
    switchMap((channelId) =>
      this._channelsProvider.getChannel(Number(channelId))
    )
  );
  public videos$ = this.channel$.pipe(
    tap(() => this.pagination$.next({ pageSize: 10, pageIndex: 0, length: 0 })),
    switchMap((channel) =>
      this.pagination$.pipe(
        switchMap((pagination) =>
          this._events.on('refresh-channel-video-list').pipe(
            startWith(''),
            switchMap(() =>
              this._videosProvider.getVideos({
                filter: `channel.id~eq~${channel.id}~AND`,
                order: 'video.createdAt~DESC~AND',
                size: pagination.pageSize,
                page: pagination.pageIndex,
              })
            )
          )
        )
      )
    )
  );

  public syncVideos(): void {
    this._events.publish('refresh-channel-video-list');
  }
}
