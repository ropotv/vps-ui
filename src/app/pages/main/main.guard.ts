import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '@vps/api';
import { AppStore } from '@vps/store';

@Injectable()
export class MainGuard implements CanActivate {
  constructor(
    private _store: AppStore,
    private _router: Router,
    private _authService: AuthService
  ) {}

  public async canActivate(): Promise<boolean> {
    if (!this._store.get.authToken) {
      this._router.navigate(['/auth']);
      return false;
    }

    await this._authService.getProfile();

    return true;
  }
}
