import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { ResourcesProvider, ScenesProvider } from '@vps/api';
import { IResource, IScene } from '@vps/models';
import { filter, map, switchMap, tap } from 'rxjs';

@Component({
  templateUrl: './resource-verify.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceVerifyComponent {
  constructor(
    private _cdr: ChangeDetectorRef,
    private _resourcesProvider: ResourcesProvider,
    private _scenesProvider: ScenesProvider,
    private _snackBar: MatSnackBar,
    private _route: ActivatedRoute
  ) {}

  public selectedScene: IScene | undefined;
  public listScenes: IScene[] = [];
  public mergedScenes: IScene[] = [];

  public resource$ = this._route.paramMap.pipe(
    map((params) => params.get('resourceId')),
    filter(Boolean),
    switchMap((resourceId) =>
      this._resourcesProvider.getResource(Number(resourceId))
    )
  );
  public scenes$ = this.resource$
    .pipe(
      switchMap((resource) =>
        this._scenesProvider.getScenes({
          filter: `resource.id~eq~${resource.id}~AND;scene.verifiedAt~isNull~true~AND`,
        })
      ),
      tap(([scenes]) => {
        this.listScenes = scenes;
        this.onSceneClick(this.listScenes[0]);
      })
    )
    .subscribe();

  private _removeSceneFromLists(scene: IScene | undefined): void {
    if (!scene) return;
    this.mergedScenes = this.mergedScenes.filter((s) => s.id !== scene.id);
    this.listScenes = this.listScenes.filter((s) => s.id !== scene.id);
    this.onSceneClick(this.listScenes[0]);
  }

  private _getNextScene(scene: IScene | undefined): IScene | undefined {
    if (!scene) return;
    const currentSceneIdx = this.listScenes.findIndex((s) => s.id === scene.id);
    return this.listScenes[currentSceneIdx + 1];
  }

  public async onScenesMerge(resource: IResource): Promise<void> {
    await this._resourcesProvider.compileScenes(resource.id, {
      sceneIds: this.mergedScenes.map((s) => s.id),
    });
    if (this.mergedScenes.some((s) => s.id === this.selectedScene?.id)) {
      this.onSceneClick(this.listScenes[0]);
    }
    this.mergedScenes = [];
    this._snackBar.open('Scenes were send in compilation queue');
    this._cdr.detectChanges();
  }

  public onSceneMerge(scene: IScene | undefined): void {
    if (!scene) return;
    const nextScene = this._getNextScene(scene);
    this.mergedScenes = [...this.mergedScenes, scene];
    this.listScenes = this.listScenes.filter((s) => s.id !== scene.id);
    this.onSceneClick(nextScene);
    this._cdr.detectChanges();
  }

  public onSceneMergeRevert(scene: IScene | undefined): void {
    if (!scene) return;
    this.listScenes = [scene, ...this.listScenes];
    this.mergedScenes = this.mergedScenes.filter((s) => s.id !== scene.id);
    this._cdr.detectChanges();
  }

  public async onSceneDelete(scene: IScene | undefined): Promise<void> {
    if (!scene) return;
    const nextScene = this._getNextScene(scene);
    await this._scenesProvider.deleteScene(scene.id);
    this._removeSceneFromLists(scene);
    this.onSceneClick(nextScene);
    this._snackBar.open('Scene was deleted successfully!');
    this._cdr.detectChanges();
  }

  public async onSceneApprove(scene: IScene | undefined): Promise<void> {
    if (!scene) return;
    const nextScene = this._getNextScene(scene);
    await this._scenesProvider.updateScene(scene.id, { isVerified: true });
    this._removeSceneFromLists(scene);
    this.onSceneClick(nextScene);
    this._snackBar.open('Scene was approved successfully');
    this._cdr.detectChanges();
  }

  public onSceneClick(scene: IScene | undefined): void {
    if (!scene) return;
    this.selectedScene = scene;
    this._cdr.detectChanges();
  }

  public handleReorder(items: any[], event: CdkDragDrop<void>): void {
    moveItemInArray(items, event.previousIndex, event.currentIndex);
  }

  public get selectedSceneInMergeList(): boolean {
    if (!this.selectedScene) return false;
    return this.mergedScenes.some((s) => s.id === this.selectedScene?.id);
  }
}
