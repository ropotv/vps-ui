import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSliderModule } from '@angular/material/slider';
import { RouterModule } from '@angular/router';
import { DialogLayoutComponent, VideoPlayerComponent } from '@vps/components';
import { StorageFileUrlPipe } from '@vps/pipes';
import { ResourceVerifyComponent } from './resource-verify.component';
import { ResourceVerifyRoutes } from './resource-verify.routes';

@NgModule({
  imports: [
    RouterModule.forChild(ResourceVerifyRoutes),
    CommonModule,
    DragDropModule,
    MatSliderModule,
    MatButtonModule,
    MatIconModule,
    DialogLayoutComponent,
    StorageFileUrlPipe,
    VideoPlayerComponent,
  ],
  declarations: [ResourceVerifyComponent],
})
export class ResourceVerifyModule {}
