import { Route } from '@angular/router';
import { ResourceVerifyComponent } from './resource-verify.component';

export const ResourceVerifyRoutes: Route[] = [
  {
    path: '',
    component: ResourceVerifyComponent,
  },
];
