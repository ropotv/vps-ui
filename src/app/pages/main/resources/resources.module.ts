import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule } from '@angular/router';
import { ResourceMenuComponent } from '@vps/menus';
import { ResourceStatusBgPipe, ResourceStatusTextPipe } from '@vps/pipes';
import { ResourcesComponent } from './resources.component';
import { ResourcesRoutes } from './resources.routes';

@NgModule({
  imports: [
    RouterModule.forChild(ResourcesRoutes),
    CommonModule,
    MatButtonModule,
    MatIconModule,
    ReactiveFormsModule,
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    MatTooltipModule,
    ResourceMenuComponent,
    ResourceStatusBgPipe,
    ResourceStatusTextPipe,
  ],
  declarations: [ResourcesComponent],
})
export class ResourcesModule {}
