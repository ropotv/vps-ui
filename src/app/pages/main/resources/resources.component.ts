import { ChangeDetectionStrategy, Component } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { ResourcesProvider } from '@vps/api';
import { AppEvents } from '@vps/events';
import { BehaviorSubject, startWith, switchMap } from 'rxjs';

@Component({
  templateUrl: './resources.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourcesComponent {
  constructor(
    private _resourcesProvider: ResourcesProvider,
    private _events: AppEvents
  ) {}

  public columns = ['resource', 'status', 'actions'];

  public pagination$ = new BehaviorSubject<PageEvent>({
    pageSize: 10,
    pageIndex: 0,
    length: 0,
  });
  public resources$ = this.pagination$.pipe(
    switchMap((pagination) =>
      this._events.on('refresh-resource-list').pipe(
        startWith(''),
        switchMap(() =>
          this._resourcesProvider.getResources({
            order: 'resource.createdAt~DESC~AND',
            size: pagination.pageSize,
            page: pagination.pageIndex,
          })
        )
      )
    )
  );

  public syncResources(): void {
    this._events.publish('refresh-resource-list');
  }
}
