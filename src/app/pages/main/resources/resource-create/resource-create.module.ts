import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { RouterModule } from '@angular/router';
import { DialogLayoutComponent } from '@vps/components';
import { ResourceCreateComponent } from './resource-create.component';
import { ResourceCreateRoutes } from './resource-create.routes';

@NgModule({
  imports: [
    RouterModule.forChild(ResourceCreateRoutes),
    CommonModule,
    MatButtonModule,
    DialogLayoutComponent,
    MatProgressSpinnerModule,
  ],
  declarations: [ResourceCreateComponent],
})
export class ResourceCreateModule {}
