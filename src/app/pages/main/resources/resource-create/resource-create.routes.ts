import { Route } from '@angular/router';
import { ResourceCreateComponent } from './resource-create.component';

export const ResourceCreateRoutes: Route[] = [
  {
    path: '',
    component: ResourceCreateComponent,
  },
];
