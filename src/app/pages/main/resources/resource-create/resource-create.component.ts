import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ResourcesProvider } from '@vps/api';
import { DialogLayoutComponent } from '@vps/components';
import { getEventDetector } from '@vps/utils';

@Component({
  templateUrl: './resource-create.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceCreateComponent {
  constructor(
    private _resourcesProvider: ResourcesProvider,
    private _snackBar: MatSnackBar
  ) {}

  @ViewChild('dialogLayout')
  private _dialogLayout: DialogLayoutComponent | undefined;

  public eventDetector = getEventDetector('refresh-resource-list');
  public isUploading = false;

  public onSubmit(videoPicker: HTMLInputElement): void {
    const videoFile = videoPicker.files?.item(0);
    if (!videoFile) {
      this._snackBar.open('Please select video to continue');
      return;
    }

    this.isUploading = true;
    this._resourcesProvider
      .createResource(videoFile)
      .then(() => {
        this.eventDetector.markAsChanged();
        this._snackBar.open('Resource uploaded successfully!');
        this._dialogLayout?.close();
      })
      .catch(() => (this.isUploading = false));
  }
}
