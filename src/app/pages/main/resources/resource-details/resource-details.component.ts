import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ResourcesProvider } from '@vps/api';
import { DialogLayoutComponent } from '@vps/components';
import { AppEvents } from '@vps/events';
import { getEventDetector } from '@vps/utils';
import { filter, map, startWith, switchMap } from 'rxjs';

@Component({
  templateUrl: './resource-details.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceDetailsComponent {
  constructor(
    private _resourcesProvider: ResourcesProvider,
    private _route: ActivatedRoute,
    private _events: AppEvents
  ) {}

  @ViewChild('dialogLayout')
  private _dialogLayout: DialogLayoutComponent | undefined;

  public eventDetector = getEventDetector('refresh-resource-list');

  public resource$ = this._route.paramMap.pipe(
    map((params) => params.get('resourceId')),
    filter(Boolean),
    switchMap((resourceId) =>
      this._events.on('refresh-resource-details').pipe(
        startWith(''),
        switchMap(() => this._resourcesProvider.getResource(Number(resourceId)))
      )
    )
  );

  public markAsChanged(): void {
    this._events.publish('refresh-resource-details');
    this.eventDetector.markAsChanged();
  }

  public closeDialog(): void {
    this.eventDetector.markAsChanged();
    this._dialogLayout?.close();
  }
}
