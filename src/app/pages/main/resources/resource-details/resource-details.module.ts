import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { DialogLayoutComponent, VideoPlayerComponent } from '@vps/components';
import { ResourceMenuComponent } from '@vps/menus';
import {
  ResourceStatusBgPipe,
  ResourceStatusTextPipe,
  StorageFileUrlPipe,
} from '@vps/pipes';
import { ResourceDetailsComponent } from './resource-details.component';
import { ResourceDetailsRoutes } from './resource-details.routes';

@NgModule({
  imports: [
    RouterModule.forChild(ResourceDetailsRoutes),
    CommonModule,
    MatButtonModule,
    MatIconModule,
    DialogLayoutComponent,
    VideoPlayerComponent,
    ResourceMenuComponent,
    StorageFileUrlPipe,
    ResourceStatusTextPipe,
    ResourceStatusBgPipe,
  ],
  declarations: [ResourceDetailsComponent],
})
export class ResourceDetailsModule {}
