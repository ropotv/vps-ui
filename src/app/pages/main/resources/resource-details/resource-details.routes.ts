import { Route } from '@angular/router';
import { ResourceDetailsComponent } from './resource-details.component';

export const ResourceDetailsRoutes: Route[] = [
  {
    path: '',
    component: ResourceDetailsComponent,
  },
];
