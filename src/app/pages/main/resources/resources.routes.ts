import { Route } from '@angular/router';
import { ResourcesComponent } from './resources.component';

export const ResourcesRoutes: Route[] = [
  {
    path: '',
    component: ResourcesComponent,
    children: [
      {
        path: 'create',
        loadChildren: () =>
          import('./resource-create/resource-create.module').then(
            (m) => m.ResourceCreateModule
          ),
      },
      {
        path: ':resourceId',
        loadChildren: () =>
          import('./resource-details/resource-details.module').then(
            (m) => m.ResourceDetailsModule
          ),
      },
      {
        path: ':resourceId/verify',
        loadChildren: () =>
          import('./resource-verify/resource-verify.module').then(
            (m) => m.ResourceVerifyModule
          ),
      },
    ],
  },
];
