import { Injectable } from '@angular/core';
import { merge, Observable, Subject } from 'rxjs';

export type ChangeType =
  | 'refresh-job-list'
  | 'refresh-resource-details'
  | 'refresh-resource-list'
  | 'refresh-scene-details'
  | 'refresh-scene-list'
  | 'refresh-thumbnail-list'
  | 'refresh-channel-list'
  | 'refresh-channel-video-details'
  | 'refresh-channel-video-list';

@Injectable({
  providedIn: 'root',
})
export class AppEvents {
  private _events = new Map<ChangeType, Subject<void>>();

  private getOrCreateEvent(type: ChangeType): Subject<void> {
    return (
      this._events.get(type) || this._events.set(type, new Subject()).get(type)!
    );
  }

  public publish(...types: ChangeType[]): void {
    types.forEach((type) => this.getOrCreateEvent(type).next());
  }

  public on(...types: ChangeType[]): Observable<void> {
    return merge(...types.map((t) => this.getOrCreateEvent(t).asObservable()));
  }
}
